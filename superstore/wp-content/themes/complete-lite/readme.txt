/*-----------------------------------------------------------------------------------*/
/* Complete Lite Responsive WordPress Theme */
/*-----------------------------------------------------------------------------------*/

Theme Name      :   Complete Lite
Theme URI       :   https://www.sktthemes.net/shop/free-premium-wordpress-theme/
Version         :   1.5
Tested up to    :   WP 4.6.1
Author          :   SKT Themes
Author URI      :   http://www.sktthemes.net/

license         :   GNU General Public License v3.0
License URI     :   http://www.gnu.org/licenses/gpl.html

/*-----------------------------------------------------------------------------------*/
/* About Author - Contact Details */
/*-----------------------------------------------------------------------------------*/

email       :   support@sktthemes.com

/*-----------------------------------------------------------------------------------*/
/* Theme Resources */
/*-----------------------------------------------------------------------------------*/


How to setup this theme?

Theme Setup : After log in to the WordPress dashboard go to [Appearance-> Theme] and active Complete Lite Theme.

1 - After activating the theme kindly visit Appearance > Customize and check following options available

Note : After change any option click on "Save & Publish" Button.

 A : Site Identity - Set the logo or title and tagline of the site.
 
 B : Colors - With this option you can change backgound color and color scheme of the site. for change background color click on 	     background color option and for change color scheme of site click on color scheme option.
 
 C : Header Image - With this option you can set header image for you current header. for add image simply click on "Add new     					     image" button choose and crop image and set header image. 
 
 D : Background Image - With this option you can set body background image for site. for add background image simply click on     "Select Image" and choose image with available option like Background Repeat, Background Position, Background Attachement.
 
 E : Menu - Select which menu would you like to use in header.
 
 F : Widgets - Manage Footer & Inner Page Sidebar Widgets Section. 
 
 G : Static Frontpage - Setting to set either your latest posts or a static page.
 
 H : Top Bar - With this option you can manage top bar left and right section.
 
 I : Home Boxes - Select page from dropdown for displaying on home below slider. Select page for box one, Select page for box two,     Select page for box three, Select page for box four. if you want to hide all these boxes simply check the option "Hide Page     Boxes".
 
 J : Slider Settings - 
 
 	 Slider Image :: Under heading "Slide Image 1, Slide Image 2, Slide Image 3" simply choose option remove image and upload     new or simply change image. and set slide image. 
	
 	 Slider Title :: Under heading "Slide title 1, Slide title 2, Slide title 3" put slide title for each slide.
	
   	 Slider Description :: Under heading "Slide description 1, Slide description 2, Slide description 3" put slide      		     description for slide.
	
 	 Slider Link :: Under heading "Slide link 1, Slide link 2, Slide link 3" put slide title for slide.		
	 
	 Slider Hide :: For hide slider section check the option "Hide This Section".
	
 K : Social Settings : With this option you can manage site footer social icons. suppose if you want to hide facebook icon so          simply make this field blank.
 
 L : Footer Area : With this option you can manage site footer about us section.
 
 M : Contact Details : With this option you can manage footer contact details.

Or for deeply information Click on Documentation and support.

Theme is Built using the following resource bundles.

1 - All js that have been used are within folder /js of theme.
- jquery.nivo.slider.js is licensed under MIT.

2 - Roboto - https://www.google.com/fonts/specimen/Roboto   
	License: Distributed under the terms of the Apache License, version 2.0 	http://www.apache.org/licenses/
	
	Lato - https://www.google.com/fonts/specimen/Lato
	Piedra - https://www.google.com/fonts/specimen/Piedra
	License: Licensed under GPL. Fonts licensed under SIL OFL 1.1 http://scripts.sil.org/OFL

3 - Social Icons
	Icons have used from Font Awesome Icons http://fortawesome.github.io/Font-Awesome/icons/
	
4 - SKT Self Design Icons	
	complete-lite/images/featured_icon1.png
	complete-lite/images/featured_icon2.png
	complete-lite/images/featured_icon3.png
	complete-lite/images/featured_icon4.png	
	complete-lite/images/sktskill.jpg
	complete-lite/images/hr_footer.png
	complete-lite/images/hr_double.png
	complete-lite/images/slide-nav.png
	complete-lite/images/mobile_nav_right.png	
	complete-lite/images/slides/slider1.jpg
	complete-lite/images/slides/slider2.jpg
	complete-lite/images/slides/slider2.jpg

For any help you can mail us at support[at]sktthemes.com