<?php

	if(!isset($_POST['dbname'])) {
		$settings = json_decode(file_get_contents('settings.json'));
		if($settings->is_wordpress) {
			?>
			<form action="?" method="post">
				<div><label for="bdname">Database Name
					<input id="bdname" value="<?= $settings->dbname ?>" type="text" name="dbname">
				</div></label>
				<div><label for="dbuser">Database User
					<input id="dbuser" value="<?= $settings->dbuser ?>" type="text" name="dbuser">
				</div></label>
				<div><label for="dbpassword">Database Password
					<input id="dbpassword" value="<?= $settings->dbpass ?>" type="text" name="dbpassword">
				</div></label>
				<div><label for="dbhost">Database Host
					<input id="dbhost" value="<?= $settings->dbhost ?>" type="text" name="dbhost">
				</div></label>
				<div><label for="dbfile">Database filename
					<input id="dbfile" value="<?= $settings->dbfile ?>" type="text" name="dbfile">
				</div></label>
				<div><input type="submit" value="submit"></div>
			</form>
			<?php
		} else {
			print "<h1>Not Wordpress - please import manually</h1>";
		}
	} else {


		// Name of the file
		$filename = $_POST['dbfile'];
		// MySQL host
		$mysql_host = $_POST['dbhost'];
		// MySQL username
		$mysql_username = $_POST['dbuser'];
		// MySQL password
		$mysql_password = $_POST['dbpassword'];
		// Database name
		$mysql_database = $_POST['dbname'];

		var_dump(shell_exec("mysql -u $mysql_username -p'$mysql_password' $mysql_database < ".__DIR__."/$filename.sql"));


	}