=== DB Tables Import/Export ===
Contributors: Bigbabert
Donate link: http://eatscode.com/
Tags: database, import, export, custom tables, database tables, export csv, import csv, export json, import json
Requires at least: 3.9
Tested up to: 4.3
Stable tag: 1.0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

DB Tables Import/Export by AlterTech allow to import/export tables data from your database.

== Description ==

<h3>DB Tables Import/Export</h3>
<p>If you are looking for a plugin to import and export data from or into your database, DB Tables Import/Export is for you.</p>
<p>Once installed and activated the plugin, two new menus will appear in the "Tools" with the pages for the import and export of tables in your database, without entering data connection or special settings.</p>
<p>The export can be done in CSV format file to JSON, while the import can be done via CSV file.</p>
<h4>This is stable version 1.0.1 and is compatible up to 4.3.1</h4>


== Installation ==

How install DB Tables Import/Export plugin:

= Using The WordPress Dashboard =

1. Navigate to the 'Add New' in the plugins dashboard
2. Search for 'DB Tables Import/Export'
3. Click 'Install Now'
4. Activate the plugin on the Plugin dashboard

= Uploading in WordPress Dashboard =

1. Navigate to the 'Add New' in the plugins dashboard
2. Navigate to the 'Upload' area
3. Select `alter-db-tables.zip` from your computer
4. Click 'Install Now'
5. Activate the plugin in the Plugin dashboard
6. Go to Tools to see the new import/Export tools

= Using FTP =

1. Download `alter-db-tables.zip`
2. Extract the `alter-db-tables` directory to your computer
3. Upload the `alter-db-tables` directory to the `/wp-content/plugins/` directory
4. Activate the plugin in the Plugin dashboard
5. Go to Tools to see the new import/Export tools


== Frequently Asked Questions ==

= Can i use this plugin with my custom tables? =

Yes You Can.

= Why i some tables return null or is only symbol in csv? =

Because this table’s data is encrypted.


== Screenshots ==

1. This screen shot is of the import section.

2. This screen shot is of the export section.

== Changelog ==

= 1.0.1 =
* Fixed PHP requirements.

= 1.0.0 =
* First stable release.

= 0.0.5 =
* First beta.

== Upgrade Notice ==

= 1.0 =
This is the first stable version if you use beta we suggest to upgrade immediately.

= 0.5 =
This is first beta tester version don't use in production.

= Support =

Need support? This is the right way to ask support:
That's the way:

* Register on http://www.blog.altertech.it 
* Send a mail to bigbabert@gmail.com with object ‘alter-db-tables’

== Updates ==

The basic structure of this plugin was cloned from the [WordPress-Plugin-Boilerplate](https://github.com/tommcfarlin/WordPress-Plugin-Boilerplate) project.